<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'HTML Minifier',
    'description' => 'This extension minifies the output of TYPO3 generated pages.',
    'category' => 'fe',
    'version' => '1.1.7',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearcacheonload' => 1,
    'author' => 'Dominik Weber',
    'author_email' => 'post@dominikweber.de',
    'author_company' => 'www.dominikweber.de',
    'constraints' => [
        'depends' => [
            'php' => '5.4.0-0.0.0',
            'typo3' => '6.0.0-9.4.99',
        ],
    ],
];
